from django.http import HttpResponse
from django.shortcuts import render

def registerCustomer(request):
    """ Exemple de page non valide au niveau HTML pour que l'exemple soit concis """
    return HttpResponse("""
        <h1>Créer un compte user</h1>
        <p>Les crêpes bretonnes ça tue des mouettes en plein vol !</p>
    """)
from django.db import models
from django.utils import timezone


class CustomerGroup(models.Model):
    name = models.CharField(max_length=120)
    date_add = models.DateTimeField(default=timezone.now)
    reduction = models.DecimalField(max_digits=4, decimal_places=2)

    class Meta:
        verbose_name = "Groupe client"

    def __str__(self):

        return self.name

class Customer(models.Model):
    first_name = models.CharField(max_length=100, verbose_name="Prénom")
    last_name = models.CharField(max_length=100, verbose_name="Nom")
    email = models.EmailField()
    registration_date = models.DateTimeField(default=timezone.now, 
                                verbose_name="Date d'inscription")
    groups = models.ManyToManyField(CustomerGroup, blank=True)
    
    class Meta:
        verbose_name = "Client"
        ordering = ['registration_date']
    
    def __str__(self):

        return self.first_name


class Address(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de l'adresse")
    first_name = models.CharField(max_length=100, verbose_name="Prénom")
    last_name = models.CharField(max_length=100, verbose_name="Nom")
    city = models.TextField(verbose_name="Ville")
    zip_code = models.CharField(max_length=15, verbose_name="Code postal")
    mailing_address = models.TextField(verbose_name="Numéro et nom de la rue")
    registration_date = models.DateTimeField(default=timezone.now, verbose_name="Date d'inscription")
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='addresses_fk')

    class Meta:
        verbose_name = "Adresse"
    
    def __str__(self):

        return self.name

    class Meta:
        verbose_name = "Commande"

    def __str__(self):
        return self.name
            

class Supplier(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField()
    date_add = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = "Fournisseur"

    def __str__(self):
        return self.name

class Article(models.Model):
    name = models.CharField(max_length=600, verbose_name='Nom')
    date_add = models.DateTimeField(default=timezone.now, verbose_name='Date d\'ajout')
    price = models.DecimalField(max_digits=6, decimal_places=2, verbose_name='Prix')
    reference = models.CharField(max_length=100, default=None, blank=True, null=True, verbose_name='Référence Fripon')
    referenceSupplier = models.CharField(max_length=100, default=None, blank=True, null=True, verbose_name='Référence Fou.')
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE, verbose_name='Fournisseur')

class Cart(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    creation_date = models.DateTimeField(default=timezone.now)

class CartProduct(models.Model):
    id_cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    id_article = models.ForeignKey(Article, on_delete=models.CASCADE)

class OrderState(models.Model):
    send_email = models.BooleanField(default=0)
    shipped = models.BooleanField(default=0)
    paid = models.BooleanField(default=0)

class Order(models.Model):
    reference = models.CharField(max_length=50)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    delivery_address = models.ForeignKey(Address, on_delete=models.CASCADE)
    invoiced_address = models.ForeignKey(Address, on_delete=models.CASCADE, related_name="order_invoice_address_fk")
    payment = models.CharField(max_length=120)
    date_add = models.DateTimeField(default=timezone.now)
    shipping_number = models.CharField(max_length=120)
    state = models.ForeignKey(OrderState, on_delete=models.CASCADE)


from django.contrib import admin

from .models import Customer, Address, Order, Supplier, CustomerGroup, CartProduct

class CustomerGroupAdmin(admin.ModelAdmin):
   list_display   = ('name', )
   date_hierarchy = 'date_add'
   ordering       = ('date_add', )
   search_fields  = ('name', )


class CustomerAdmin(admin.ModelAdmin):
   list_display   = ('first_name', 'last_name', 'email')
   date_hierarchy = 'registration_date'
   ordering       = ('registration_date', )
   search_fields  = ('first_name', 'last_name')

class AddressAdmin(admin.ModelAdmin):
   list_display   = ('name', 'zip_code', 'city')
   date_hierarchy = 'registration_date'
   ordering       = ('registration_date', )
   search_fields  = ('first_name', 'last_name')

class OrderAdmin(admin.ModelAdmin):
  list_display = ('reference',)
  date_hierarchy = 'date_add'
  search_fields = ('reference', )

class CartProductAdmin(admin.ModelAdmin):
  list_display = ('id_cart',)
  search_fields = ('id_cart', )

class SupplierAdmin(admin.ModelAdmin):
  list_display = ('name',)
  date_hierarchy = 'date_add'
  search_fields = ('name', )

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Supplier, SupplierAdmin)
admin.site.register(CustomerGroup, CustomerGroupAdmin)
admin.site.register(CartProduct, CartProductAdmin)

from django import forms
from boutique.models import Customer, CustomerGroup, Supplier, Article, Cart, Order, Address

class LoginForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        exclude = ['registration_date']

class CustomerGroupForm(forms.ModelForm):
    class Meta:
        model = CustomerGroup
        fields = '__all__'

class SupplierForm(forms.ModelForm):
	class Meta:
		model = Supplier
		fields = '__all__'

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = '__all__'
class CartForm(forms.ModelForm):
    class Meta:
        model = Cart
        fields = '__all__'
class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ['reference', 'date_add']

class AddressForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ['date_add']
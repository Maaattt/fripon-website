from django.http import HttpResponse, Http404
from datetime import datetime
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .forms import LoginForm, CustomerForm, CustomerGroupForm, SupplierForm, ArticleForm, CartForm, OrderForm
from boutique.models import Customer, CustomerGroup, Supplier, Article, Cart, Order, Address


@login_required
def home(request):
	return render(request, 'panel/home.html', locals())

@login_required
def boutique(request):
    return render(request, 'panel/boutique.html', locals())



## VUES DES COMMANDES ##


@login_required
def orders(request):
    orders = Order.objects.all()
    return render(request, 'panel/orders.html', locals())

@login_required
def printOrder(request, id):
    try:
        order = Order.objects.get(id=id)
        if request.method == "POST":
            form = OrderForm(request.POST, instance=order)
            if form.is_valid():
                form.save()
                return redirect(reverse(orders))
        else:
            form = OrderForm(instance=order)
       
    except Order.DoesNotExist:
        raise Http404
    return render(request, 'panel/printOrder.html', locals())

@login_required
def addOrder(request):
    if request.method == "POST":
        form = OrderForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse(orders))
    else:
        form = OrderForm()
       
    return render(request, 'panel/addOrder.html', locals())

@login_required
def delOrder(request, id):
    try:
        order = Order.objects.get(id=id)
        form = OrderForm(instance=order)
        if form.is_valid():
            form.save()
        order.delete()
    except OrderForm.DoesNotExist:
        raise Http404
    return redirect(reverse(orders))

## VUES DES PANIERS ##


@login_required
def carts(request):
    carts = Cart.objects.all()
    return render(request, 'panel/carts.html', locals())

@login_required
def printCart(request, id):
    try:
        cart = Cart.objects.get(id=id)
        if request.method == "POST":
            form = CartForm(request.POST, instance=cart)
            if form.is_valid():
                form.save()
                return redirect(reverse(carts))
        else:
            form = CartForm(instance=cart)
       
    except Cart.DoesNotExist:
        raise Http404
    return render(request, 'panel/printCart.html', locals())

@login_required
def addCart(request):
    if request.method == "POST":
        form = CartForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse(carts))
    else:
        form = CartForm()
       
    return render(request, 'panel/addCart.html', locals())

@login_required
def delCart(request, id):
    try:
        cart = Cart.objects.get(id=id)
        form = CartForm(instance=cart)
        if form.is_valid():
            form.save()
        cart.delete()
    except CartForm.DoesNotExist:
        raise Http404
    return redirect(reverse(carts))


## VUES DES ARTICLES ##


@login_required
def articles(request):
    articles = Article.objects.all()
    return render(request, 'panel/articles.html', locals())

@login_required
def printArticle(request, id):
    try:
        article = Article.objects.get(id=id)
        if request.method == "POST":
            form = ArticleForm(request.POST, instance=article)
            if form.is_valid():
                form.save()
                return redirect(reverse(articles))
        else:
            form = ArticleForm(instance=article)
       
    except Article.DoesNotExist:
        raise Http404
    return render(request, 'panel/printArticle.html', locals())

@login_required
def addArticle(request):
    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse(articles))
    else:
        form = ArticleForm()
       
    return render(request, 'panel/addArticle.html', locals())

@login_required
def delArticle(request, id):
    try:
        article = Article.objects.get(id=id)
        form = ArticleForm(instance=article)
        if form.is_valid():
            form.save()
        article.delete()
    except Article.DoesNotExist:
        raise Http404
    return redirect(reverse(articles))



## VUES DES FOURNISSEURS ##
@login_required
def suppliers(request):
    suppliers = Supplier.objects.all()
    return render(request, 'panel/suppliers.html', locals())

@login_required
def printSupplier(request, id):
    try:
        supplier = Supplier.objects.get(id=id)
        if request.method == "POST":
            form = SupplierForm(request.POST, instance=supplier)
            if form.is_valid():
                form.save()
                return redirect(reverse(suppliers))
        else:
            form = SupplierForm(instance=supplier)
       
    except Customer.DoesNotExist:
        raise Http404
    return render(request, 'panel/printSupplier.html', locals())

@login_required
def addSupplier(request):
    if request.method == "POST":
        form = SupplierForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse(suppliers))
    else:
        form = SupplierForm()
       
    return render(request, 'panel/addSupplier.html', locals())

@login_required
def delSupplier(request, id):
    try:
        supplier = Supplier.objects.get(id=id)
        form = SupplierForm(instance=supplier)
        if form.is_valid():
            form.save()
        supplier.delete()
    except Supplier.DoesNotExist:
        raise Http404
    return redirect(reverse(suppliers))



## VUES DES GROUPES CLIENTS ##
@login_required
def customergroups(request):

    customergroups = CustomerGroup.objects.all()
    return render(request, 'panel/customergroups.html', {'customergroups' : customergroups}, locals())

@login_required
def addCustomerGroup(request):
    if request.method == "POST":
        form = CustomerGroupForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse(customergroups))
    else:
        form = CustomerGroupForm()
       
    return render(request, 'panel/addCustomerGroup.html', locals())

@login_required
def printCustomergroup(request, id):
    try:
        customergroup = CustomerGroup.objects.get(id=id)
        if request.method == "POST":
            form = CustomerGroupForm(request.POST, instance=customergroup)
            if form.is_valid():
                form.save()
                return redirect(reverse(customergroups))
        else:
            form = CustomerGroupForm(instance=customergroup)
       
    except Customer.DoesNotExist:
        raise Http404
    return render(request, 'panel/printCustomerGroup.html', locals())

##MANQUE LE ADD GROUP 

@login_required
def delCustomerGroup(request, id):
    try:
        customerGroup = CustomerGroup.objects.get(id=id)
        form = CustomerForm(instance=customerGroup)
        if form.is_valid():
            form.save()
        customerGroup.delete()
    except CustomerGroup.DoesNotExist:
        raise Http404
    return redirect(reverse(customergroups))


## VUES DES CLIENTS ##


@login_required
def clients(request):

    customers = Customer.objects.all()
    return render(request, 'panel/clients.html', {'customers' : customers}, locals())

@login_required
def printClient(request, id):
    try:
        customer = Customer.objects.get(id=id)
        if request.method == "POST":
            form = CustomerForm(request.POST, instance=customer)
            if form.is_valid():
                form.save()
                return redirect(reverse(clients))
        else:
            form = CustomerForm(instance=customer)
       
    except Customer.DoesNotExist:
        raise Http404
    return render(request, 'panel/printClient.html', locals())

@login_required
def addClient(request):
    if request.method == "POST":
        form = CustomerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse(clients))
    else:
        form = CustomerForm()
       
    return render(request, 'panel/addClient.html', locals())

@login_required
def delClient(request, id):
    try:
        customer = Customer.objects.get(id=id)
        form = CustomerForm(instance=customer)
        if form.is_valid():
            form.save()
        customer.delete()
    except Customer.DoesNotExist:
        raise Http404
    return redirect(reverse(clients))

## VUES DES ADRESSES ##


@login_required
def addresses(request):

    addresses = Address.objects.all()
    return render(request, 'panel/addresses.html', {'addresses' : addresses}, locals())

@login_required
def printAddress(request, id):
    try:
        address = Address.objects.get(id=id)
        if request.method == "POST":
            form = AddressForm(request.POST, instance=address)
            if form.is_valid():
                form.save()
                return redirect(reverse(addresses))
        else:
            form = AddressForm(instance=address)
       
    except Customer.DoesNotExist:
        raise Http404
    return render(request, 'panel/printAddress.html', locals())

@login_required
def addAddress(request):
    if request.method == "POST":
        form = AddressForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse(addresses))
    else:
        form = AddressForm()
       
    return render(request, 'panel/addAddress.html', locals())

@login_required
def delAddress(request, id):
    try:
        address = Address.objects.get(id=id)
        form = AddressForm(instance=adress)
        if form.is_valid():
            form.save()
        address.delete()
    except Address.DoesNotExist:
        raise Http404
    return redirect(reverse(adresses))

def logIn(request):
    error = False

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)  # Nous vérifions si les données sont correctes
            if user:  # Si l'objet renvoyé n'est pas None
                login(request, user)  # nous connectons l'utilisateur
                return redirect(reverse(home))
            else: # sinon une erreur sera affichée
                error = True
    else:
        form = LoginForm()

    return render(request, 'panel/login.html', locals())


def logOut(request):
    logout(request)
    return redirect(reverse(logIn))
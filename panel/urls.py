from django.urls import path
from django.conf.urls import url, include
from . import views

urlpatterns = [
   path('', views.home, name="home"),
   path('boutique', views.boutique, name="boutique"),
   path('boutique/articles', views.articles, name="boutique"),


   ## URLS ARTICLES ## 
   path('boutique/articles', views.articles, name='boutique_articles'),
   path('boutique/articles/<int:id>', views.printArticle, name='boutique_printArticle'),
   path('boutique/addarticle', views.addArticle, name='boutique_addArticle'),
   path('boutique/delarticle/<int:id>', views.delArticle, name='boutique_delArticle'),

   ## URLS CLIENTS
   path('boutique/clients', views.clients, name="boutique_customers"),
   path('boutique/clients/<int:id>', views.printClient, name='boutique_viewClient'),
   path('boutique/addclient', views.addClient, name='boutique_addClient'),
   path('boutique/delclient/<int:id>', views.delClient, name='boutique_delClient'),

   ## URLS ADRESSES
   path('boutique/addresses', views.addresses, name="boutique_addresses"),
   path('boutique/addresses/<int:id>', views.printAddress, name='boutique_viewAddress'),
   path('boutique/addaddress', views.addAddress, name='boutique_addAddress'),
   path('boutique/deladdress/<int:id>', views.delAddress, name='boutique_delAddress'),

   ## URLS COMMANDES ## 
   path('boutique/orders', views.orders, name='boutique_orders'),
   path('boutique/orders/<int:id>', views.printOrder, name='boutique_printOrder'),
   path('boutique/addorder', views.addOrder, name='boutique_addOrder'),
   path('boutique/delorder/<int:id>', views.delOrder, name='boutique_delOrder'),

   ## URLS PANIERS
   path('boutique/carts', views.carts, name="boutique_carts"),
   path('boutique/carts/<int:id>', views.printCart, name='boutique_viewCart'),
   path('boutique/addcart', views.addCart, name='boutique_addCart'),
   path('boutique/delcart/<int:id>', views.delCart, name='boutique_delCart'),


   ## URLS GROUPES CLIENTS 
   path('boutique/customergroups', views.customergroups, name='boutique'),
   path('boutique/customergroups/<int:id>', views.printCustomergroup, name='boutique_printCustomerGroup'),
   path('boutique/addcustomergroup/', views.addCustomerGroup, name='boutique_addCustomerGroup'),
   path('boutique/delcustomergroup/<int:id>', views.delCustomerGroup, name='boutique_delCustomerGroup'),

   ## URLS FOURNISSEURS ## 
   path('boutique/suppliers', views.suppliers, name='boutique_suppliers'),
   path('boutique/suppliers/<int:id>', views.printSupplier, name='boutique_printSupplier'),
   path('boutique/delSupplier/<int:id>', views.delSupplier, name='boutique_delSupplier'),
   path('boutique/addsupplier', views.addSupplier, name='boutique_addSupplier'),


   ## URLS AUTH
   url(r'^login$', views.logIn, name='login'),
   url(r'^logout$', views.logOut, name='logout'),
]
